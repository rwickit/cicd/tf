# Terraform

This project contains a GitLab-CI Pipeline for the delivery and deployment of Terraform Templates and Modules that can be reused across multiple projects.

The sole purpose of this project is to be able to run an identical pipeline across all projects that deploy Terraform from GitLab.

Changes to this project will be tagged and versions to current use of this pipeline in remote projects will not be affected. Updates and enhancements to this pipeline will be included and tagged as a new version.

## Prerequisite(s)

Prior to using this CI Pipeline, the following configurations must be completed.

### GitLab OIDC IAM Idp/Role Pair

It is required to have a GitLab OIDC IdP and Role deployed into your AWS Account(s).

To easily complete this deployment please visit:

[https://gitlab.com/rwickit/cicd/aws-gitlab-cicd](https://gitlab.com/rwickit/cicd/aws-gitlab-cicd).

### GitLab CICD Variables

Provide GitLab Variables with the output role names from the previous step.

The CICD Variables can be placed at individual project level or any group or sub-group above the current project. However, if any lower level group variable match the variable name, they will take precedent.

Details about GitLab Variables: [https://docs.gitlab.com/ee/ci/variables/](https://docs.gitlab.com/ee/ci/variables/).

## Artifacts

The resources included in this project are:

- CI: Individual AWS Job includes
- SCA: Static Code Analysis Jobs
- Examples: .gitlab-ci.yml configurations for us in remote projects

Additional directories may be included in the future to accommodate additions or modifications to new versions of the pipeline.

## Implementation

To implement this remote pipeline into an existing project, simply replace or create the `.gitlab-ci.yml` with the following configurations.

### Static Code Analysis (SCA)

This at a minimum will run SCA on your current Terraform project.

```yml
include:
  - project: rwickit/cicd/tf
    ref: v1 ## The branch, tag, or SHA to use  (default: main)
    file: review.yml ## The path to the file to include (default: main.yml)

variables:
  ## PROJECT VARIABLES
  SOURCE_DIR: ${CI_PROJECT_DIR}/terraform/

validate:
  stage: validate
  extends: .validate

format:
  stage: format
  extends: .format
```

### SCA & Plan

This will run SCA and deliver all contents of the `SOURCE_DIR` to the s# configuration in variables.

```yml
include:
  - project: rwickit/cicd/tf
    ref: v1 ## The branch, tag, or SHA to use  (default: main)
    file: review.yml ## The path to the file to include (default: main.yml)

variables:
  ## PROJECT VARIABLES
  SOURCE_DIR: ${CI_PROJECT_DIR}/terraform/
  ROLE_SESSION_NAME: ACB-GitLab-Testing

## JOBS:

format:
  stage: format
  extends: .format

plan:
  stage: plan
  needs: [validate, format]
  extends: .plan

build:
  stage: build
  extends: .build
```

### Plan, Apply, and Destroy

This will run SCA and deliver all contents of the `SOURCE_DIR` to the s# configuration in variables.

```yml
include:
  - project: rwickit/cicd/tf
    ref: v1 ## The branch, tag, or SHA to use  (default: main)
    file: review.yml ## The path to the file to include (default: main.yml)

variables:
  ## PROJECT VARIABLES
  SOURCE_DIR: ${CI_PROJECT_DIR}/terraform/
  ROLE_SESSION_NAME: ACB-GitLab-Testing

## JOBS:

validate:
  stage: validate
  extends: .validate

format:
  stage: format
  extends: .format

plan:
  stage: plan
  needs: [validate, format]
  extends: .plan

build:
  stage: build
  extends: .build

apply:
  stage: apply
  needs: [build]
  extends: .apply

destroy:
  stage: apply
  needs: [apply]
  extends: .destroy
```

## Example CI Templates

View Example `.gitlab-ci.yml` templates in the [examples](examples/) directory.

To review and example project with a completed and successful `deploy` pipeline, please review [https://gitlab.com/rwickit/demos/ci-and-cd-on-amazon-web-service-aws/gitlab-aws-terraform](https://gitlab.com/rwickit/demos/ci-and-cd-on-amazon-web-service-aws/gitlab-aws-terraform).

## Using the Terraform Provider

This option also required the deployment of an IdP/Role Pair which can be done with the shared resource above however, the authentication to AWS is done via the Terraform Provider instead of AWS STS CLI in the [ci](ci/) directory. Some other modifications to the local CI may be required to implement the different Terraform commands for `plan`, `apply`, and `destroy`. **Updates to come in the future**

If using Terraform, an option is to use the provider `assume_role_with_identity` in the provider to assume the role.

This resource is documented here:
[https://registry.terraform.io/providers/hashicorp/aws%20%20/latest/docs#assume_role_with_web_identity-configuration-block
](https://registry.terraform.io/providers/hashicorp/aws%20%20/latest/docs#assume_role_with_web_identity-configuration-block
)

### TF Provider Example

```json
provider "aws" {
  region = var.region
  assume_role_with_web_identity {
    web_identity_token = var.deploy_web_identity_token
    role_arn           = var.assumable_role_arn
    session_name       = var.role_session_name
    duration           = var.session_duration
  }
}
```

`.gitlab-ci.yml variables presented as:`

```bash
.Terraform: &terra-base
  TF_VAR_region: $AWS_DEFAULT_REGION
  TF_VAR_web_identity_token: $CI_JOB_JWT_V2
  TF_VAR_assumable_role_arn: $AWS_DEPLOY_ROLE_ARN
  TF_VAR_role_session_name: "Descriptive-Session-Name"
  TF_VAR_session_duration: "15m"
```